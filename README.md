# yyc-tools

#### 介绍
个人常用工具集合


#### 安装教程

```
composer require fashion/yyctools
```

#### 数组工具

```
use yyctools\Utils\ArrayUtils
#获取二维数组重复值
ArrayUtils::get_two_array_repeat_values([["a" => 1,"b" => 2],["a" => 3,"b" => 3]],"a");
```

#### 日期工具

```
use yyctools\Utils\DateUtils
#根据日期获取周几
DateUtils::get_week_day("2023-10-01");
```

#### 转树形工具

```
use yyctools\Utils\ListToTreeUtils
#转成树形
ListToTreeUtils::ListToTreeRecursive([]);
```
#### 数字工具

```
use yyctools\Utils\MathUtils
#英文数字转中文数字
MathUtils::num_to_word(102);
```
#### pdf工具

```
use yyctools\Utils\PdfUtils
#html转pdf
PdfUtils::create_contract_pdf("<html>......","/tmp/202301/","测试.pdf");
```
#### 手机号码工具

```
use yyctools\Utils\PhoneUtils
#手机号码加*
PhoneUtils::encrypt_phone("13111111111");
```
#### UUID工具

```
use yyctools\Utils\UuidUtils
#获取uuid
UuidUtils::uuid();
```

### 使用必读
使用时请遵守法律法规，请勿来在非法应用中使用，产生的一切后果和法律责任均与作者无关！