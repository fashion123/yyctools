<?php

namespace yyctools\Utils;

class ListToTreeUtils
{
    /**
     * 采用递归将数据列表转换成树
     *
     * @param $dataArr           数据列表
     * @param int $rootId        根节点ID
     * @param string $pkName     主键名
     * @param string $pIdName    父节点id名
     * @param string $childName  子节点名称
     * @return array
     */
    public static function list_to_tree_recursive($dataArr, $rootId = 0, $pkName = 'id', $pIdName = 'parent_id', $childName = 'childs')
    {
        $arr = [];
        foreach ($dataArr as $sorData) {


            if ($sorData[$pIdName] == $rootId) {
                $children = self::list_to_tree_recursive($dataArr, $sorData[$pkName],$pkName,$pIdName,$childName);
                if($children){
                    $sorData[$childName] = $children;
                }
                $arr[] = $sorData;
            }

        }

        return $arr;
    }


}