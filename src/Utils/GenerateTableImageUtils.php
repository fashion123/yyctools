<?php

namespace yyctools\Utils;

class GenerateTableImageUtils
{

    public function generate($title,$column_data,$data,$font="",$file_path = '',$is_spin=200,$angle = 90){


        $font_size = 16; //字体大小
        if(empty($font)) $font = dirname(__FILE__).'/Fonts/yahei.ttf';

        // 标题长度
        $this_title_box = imagettfbbox($font_size, 0, $font, $title);
        $title_x_len = $this_title_box[2] - $this_title_box[0];
        $title_height = 30;

        // 每行高度
        $row_hight = $title_height - 10;

        // 最长宽度作表格宽度
        $column_max_box = [];
        $column_box = [];
        $text_size = 16;
        $column_data_tmp = [];
//        foreach ($column_data as $key => $datum){
//            $column_data_tmp["title_".$key] = $datum;
//        }
        $data = array_merge([$column_data],$data);
        foreach ($data as $key => $value) {
            foreach ($value as $filed => $item){
                # imagettfbbox 获取文字 4 个角的坐标
                $this_box = imagettfbbox($text_size, 0, $font, $item);
                $box_val = ($this_box[2] - $this_box[0])  ;
                $column_max_box[$filed."_x_len"] = max($box_val, ($column_max_box[$filed."_x_len"] ?? 20));
                // 每列x轴长度
                $column_box[$key][$filed."_x_len"] = $box_val;
            }
        }


        // 列数
        $column = count($data[0]);
        // 文本左右内边距
        $x_padding = 50;
        $y_padding = 10;
        // 图片宽度（每列宽度 + 每列左右内边距）
        $img_width = $x_padding * $column * 2;
        foreach ($column_max_box as $box){
            $img_width += $box;
        }
        // 图片高度（标题高度 + 每行高度 + 每行内边距）
        $img_height = $title_height + count($data) * ($row_hight + $y_padding);

        # 开始画图
        // 创建画布
        $img = imagecreatetruecolor($img_width, $img_height);


        # 创建画笔
        // 背景颜色（蓝色）
        $bg_color = imagecolorallocate($img, 24, 98, 229);
        // 表面颜色（浅灰）
        $surface_color = imagecolorallocate($img, 235, 242, 255);
        // 标题字体颜色（白色）
        $title_color = imagecolorallocate($img, 255, 255, 255);
        // 内容字体颜色（灰色）
        $text_color = imagecolorallocate($img, 152, 151, 152);

        // 画矩形 （先填充一个黑色的大块背景，小一点的矩形形成外边框）
        imagefill($img, 0, 0, $bg_color);
        imagefilledrectangle($img, 2, $title_height, $img_width - 3, $img_height - 3, $surface_color);

        $_total_box = 0;
        $_padding_x = 1;
        foreach ($column_max_box as $box){
            // 画竖线
            imageline($img, $_total_box + $box  + $x_padding * (2*$_padding_x), $title_height, $_total_box + $box + $x_padding * (2*$_padding_x), $img_height, $bg_color);
            $_padding_x ++;
            $_total_box += $box;
        }

        // 写入标题
        imagettftext($img, $font_size, 0, $img_width / 2 - $title_x_len / 2, $title_height - $font_size / 2, $title_color, $font, $title);

        // 写入表格
        $temp_height = $title_height;
        foreach ($data as $key => $value) {
            # code...
            $temp_height += $row_hight + $y_padding;
            // 画线
            imageline($img, 0, $temp_height, $img_width, $temp_height, $bg_color);
            $_total_x_len = 0;
            $x_padding_x = 1;
            foreach ($value as $filed => $item){
                $_x_len = $column_max_box[$filed."_x_len"];
                $filed_x_len = $column_box[$key][$filed."_x_len"];
                // 写入字段
                imagettftext($img, $text_size, 0, $_x_len / 2 - $filed_x_len / 2 + ($x_padding * $x_padding_x) + $_total_x_len, $temp_height - $text_size / 2, $text_color, $font, $item);
                $_total_x_len += $_x_len;
                $x_padding_x += 2;
            }

        }

        if($is_spin == 100){
            //顺时针旋转
            $img = imagerotate($img, $angle, 0);
        }

        //打开缓冲区获取图片二进制数据
        ob_start ();
        //输出图片
        imagepng($img);
        //得到缓冲区的内容
        $image_data = ob_get_contents ();
        //删除内部缓冲区的内容，并且关闭内部缓冲区
        ob_end_clean();
        //销毁图形
        imagedestroy($img);

        return 'data:image/png;base64,'.base64_encode($image_data);
    }
}