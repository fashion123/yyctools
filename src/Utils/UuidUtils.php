<?php


namespace yyctools\Utils;


class UuidUtils
{
    /**
     * 生成uuid
     * @return string
     */
    public static function   uuid()  {
        $chars = md5(uniqid(mt_rand(), true));
        $uuid = substr ( $chars, 0, 8 ) . '-'
            . substr ( $chars, 8, 4 ) . '-'
            . substr ( $chars, 12, 4 ) . '-'
            . substr ( $chars, 16, 4 ) . '-'
            . substr ( $chars, 20, 12 );
        return $uuid ;
    }

    /**
     * 生成唯一ID
     * @param $prefix
     * @return false|string
     */
    function generate_unique_id($prefix = '') {
        $microtime = microtime(true);
        $rand_num = mt_rand();
        $unique_id = uniqid($prefix, false);
        $hash = hash('sha256', $microtime . $rand_num . $unique_id);
        return substr($hash, 0, 32);
    }

}