<?php


namespace yyctools\Utils;


class PhoneUtils
{

    /**
     * 手机号码中间带*
     * @param $phone 手机号码
     * @return array|string|string[]|null
     */
    public static function encrypt_phone($phone){
        return preg_replace('/(\d{3})\d{4}(\d*)/', '$1****$2', $phone);
    }

    /**
     * 验证是否手机号码
     * @param $mobile 手机号码
     * @return bool
     */
    public static function validate_mobile($mobile) {
        //不是11位数
        if (strlen($mobile) != 11) {
            return false;
        }
        //正常的13911111111
        //0开头的 020110
        //带"-"  139-1111-1111

        if (!preg_match('/^1[3-9]\d{9}$/', $mobile)
            && !preg_match('/^0\d{2,3}\d{7,8}$/', $mobile)
            && !preg_match('/^(\d{3}-|\d{4}-)?\d{7,8}$/', $mobile)) {
            return false;
        }


        return true;
    }

    /**
     * 验证手机号码
     * @param $phone 手机号码
     * @return bool
     */
    public static function verify_phone($phone){
        if(!$phone) return false;
        if(strlen($phone) != 11 )return false;
        if(!is_numeric($phone)) return false;
        if(preg_match("/^1[3456789]{1}\d{9}$/",$phone)){
            return true;
        }
        return false;
    }
}