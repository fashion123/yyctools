<?php

namespace yyctools\Utils;
class ArrayUtils
{
    /**
     * 获取 二维数组重复值
     * @param $array  数组
     * @param $column_key 字段
     * @return array
     */
    public static function get_two_array_repeat_values($array,$column_key){
        $repeat_array = array_column($array,$column_key);
        $repeat_ret = array_filter(array_count_values($repeat_array), function($value) {
            return $value > 1;
        });
        $data = [];
        if(!empty($repeat_ret)){
            foreach ($array as $key => $item){
                if(is_array($item)){
                    if(isset($repeat_ret[$item[$column_key]])){
                        $data[] = $item;
                    }
                }
            }
        }
        return $data;

    }

    /**
     * 判断一个一维数组是否存在另外一个一维数组
     * @param $arr 数组
     * @param $allArr 数组
     * @return bool|void
     */
    public static function is_all_exists($arr, $allArr){
        if (!empty($arr) && !empty($allArr)) {
            for ($i = 0; $i < count($arr); $i++) {
                if (!in_array($arr[$i], $allArr)) {
                    return false;
                }
            }
            return true;
        }
    }


    /**
     * 二维数组去重
     * @param $array 数组
     * @param $field 字段
     * @return array|false
     */
    public static function arrayUnique($array,$field)
    {
        if(empty($array) || !$field){
            return false;
        }
        //返回指定字段的一列数据
        $fields = array_column($array, $field);
        //去重
        $fields = array_unique($fields);
        //比较两个数组，返回交集，只比较键(key)
        $data   = array_intersect_key($array,$fields);
        //重置数组的键(key)
        $data   = array_values($data);
        return $data;
    }

    /**
     * 对象 转 数组
     *
     * @param object $obj 对象
     * @return array
     */
    public static function object_to_array($obj) {
        $obj = (array)$obj;
        foreach ($obj as $k => $v) {
            if (gettype($v) == 'resource') {
                return;
            }
            if (gettype($v) == 'object' || gettype($v) == 'array') {
                $obj[$k] = (array)self::object_to_array($v);
            }
        }
        return $obj;
    }


    /**
     * 对象转数组
     * @param $obj 对象或者数组对象
     * @return array|mixed
     */
    public static function obj_to_array($obj){
        if (gettype($obj) == 'object' || is_array($obj)) {
            return json_decode(json_encode($obj,JSON_UNESCAPED_UNICODE),true);
        }
        return [];
    }

    /**
     * 数组转对象
     * @param $array 数组
     * @return mixed|null
     */
    public static function array_to_obj($array){
        if(!is_array($array)) return null;
        return json_decode(json_encode($array,JSON_UNESCAPED_UNICODE)) ?? null;

    }


    /**
     * 二维数组 根据key值找到重复值
     * @param array $array 数组
     * @param $column_key 字段
     * @return array
     */
    function get_array_repeat_values(array $array,$column_key){
        $repeat_array = array_column($array,$column_key);
        $repeat_ret = array_filter(array_count_values($repeat_array), function($value) {
            return $value > 1;
        });
        $data = [];
        if(!empty($repeat_ret)){
            foreach ($array as $key => $item){

                if(is_array($item)){
                    if(isset($repeat_ret[$item[$column_key]])){
                        $data[] = $item;
                        continue;
                    }
                }else{
                    if(isset($repeat_ret[$key])){
                        $data[] = $item;
                        continue;
                    }
                }
            }
        }
        return $data;
    }

    /**
     * 一维数组，找出中重复值和重复值出现的次数
     * @param $fruits 字段
     * @return void
     */
    public static function get_array_repeat_count($fruits){
        $result = array_filter(array_count_values($fruits), function($value) {
            return $value > 1;
        });
        return $result;
    }

    /**
     * 转成数组
     * @param $value 值
     * @return array
     */
    public static function wrap($value)
    {
        if (is_null($value)) {
            return [];
        }
        return is_array($value) ? $value : [$value];
    }

    /**
     * 将多个数组或者字符串合并成一个数组
     * @param ...$groups 多个数组
     * @return mixed
     */
    public static function groupBy(...$groups)
    {
        foreach ($groups as $group) {
            $_groups = array_merge(
                (array) $_groups,
                wrap($group)
            );
        }

        return $_groups;
    }

    /**
     * 二维数组元素求和
     * @param array $array 数组
     * @return float|int
     */
    public static function get_array_sum(array $array,$key = 0){
        $total = array_sum(array_map(function($sub_array) use($key) {
            return $sub_array[$key];
        }, $array));
        return $total;
    }

    /**
     * 多维数组 重置key
     * @param $array 数组
     * @return array|mixed
     */
    public static function reform_keys($array){
        if(!is_array($array)){
            return $array;
        }
        $keys = implode('', array_keys($array));
        if(is_numeric($keys)){
            $array = array_values($array);
        }
        $array = array_map( 'self::reform_keys', $array);
        return $array;
    }



}